from flask import Flask
from flask import render_template, redirect, url_for,request,jsonify
import os


app = Flask(__name__)

@app.route("/home",methods=['GET','POST'])
def home():
    var1 = "world"
    return f"hello{var1}"

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)
